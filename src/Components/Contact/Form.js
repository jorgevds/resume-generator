import React from "react";
import { Formik } from "formik";

const Form = () => (
  <section className="app-contact" id="contact" data-testid="form">
    <article className="letter-text form-title">
      <h1>Contact me!</h1>
    </article>
    <Formik
      initialValues={{ email: "", message: "" }}
      validate={(values) => {
        const errors = {};
        if (!values.email) {
          errors.email = "Hey, ik wil je wel terugmailen, hé";
        } else if (
          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
        ) {
          errors.email = "Hmm, dat klopt hier gelijk niet";
        }
        if (!values.message) {
          errors.message = "Wil je niet met mij spreken?";
        }
        return errors;
      }}
      onSubmit={(values, { setSubmitting }) => {
        setTimeout(() => {
          alert(JSON.stringify(values, null, 2));
          setSubmitting(false);
        }, 400);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form onSubmit={handleSubmit}>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            name="email"
            id="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
            placeholder="Emailadres"
            className={
              (touched.message && errors.message) ||
              (touched.email && errors.email)
                ? "danger"
                : "success"
            }
          />
          <legend data-testid="errors-email">
            {touched.email && errors.email}
          </legend>
          <label htmlFor="message">Bericht</label>
          <textarea
            name="message"
            id="message"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.message}
            placeholder="Schrijf een berichtje..."
            className={
              (touched.message && errors.message) ||
              (touched.email && errors.email)
                ? "danger"
                : "success"
            }
          />
          <legend data-testid="errors-message">
            {touched.message && errors.message}
          </legend>
          <button
            type="submit"
            disabled={isSubmitting}
            className={
              (touched.message && errors.message) ||
              (touched.email && errors.email)
                ? "danger"
                : "success"
            }
          >
            Verzend
          </button>
        </form>
      )}
    </Formik>
    <article className="form-blurb">
      Laat hier je emailadres en een berichtje achter. Ik volg zo snel mogelijk
      op met een antwoord!
    </article>
  </section>
);

export default Form;
