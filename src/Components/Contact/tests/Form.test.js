import React from "react";
import {
  render,
  screen,
  fireEvent,
  act,
  waitFor,
} from "@testing-library/react";
import Form from "../Form";

test("Renders components", () => {
  render(<Form />);
  const form = screen.getByTestId("form");
  expect(form).toBeInTheDocument();
});

test("Form input should validate on blur", async () => {
  const { getByLabelText, getByTestId } = render(<Form />);
  const input = getByLabelText("Email");

  act(() => {
    fireEvent.blur(input);
  });

  await waitFor(() => {
    expect(getByTestId("errors-email")).not.toBe(null);
    expect(getByTestId("errors-email")).toHaveTextContent(
      "Hey, ik wil je wel terugmailen, hé"
    );
  });
});

test("Form text area should validate on blur", async () => {
  const { getByLabelText, getByTestId } = render(<Form />);
  const input = getByLabelText("Bericht");

  act(() => {
    fireEvent.blur(input);
  });

  await waitFor(() => {
    expect(getByTestId("errors-message")).not.toBe(null);
    expect(getByTestId("errors-message")).toHaveTextContent(
      "Wil je niet met mij spreken?"
    );
  });
});

test("Form input should not throw on change", async () => {
  const { getByLabelText, queryByText } = render(<Form />);

  const input = getByLabelText("Email");
  act(() => {
    fireEvent.change(input, { target: { value: "jorge@test.be" } });
  });

  await waitFor(() => {
    expect(
      queryByText("Hey, ik wil je wel terugmailen, hé")
    ).not.toBeInTheDocument();
  });
});

test("Form text area should not throw on change", async () => {
  const { getByLabelText, queryByText } = render(<Form />);

  const input = getByLabelText("Bericht");
  act(() => {
    fireEvent.change(input, {
      target: { value: "jorge is de beste programmeur ooit" },
    });
  });

  await waitFor(() => {
    expect(queryByText("Wil je niet met mij spreken?")).not.toBeInTheDocument();
  });
});

test("Form should not throw on submit", async () => {
  const { getByLabelText, getByText, queryByText } = render(<Form />);

  const input = getByLabelText("Email");
  const textarea = getByLabelText("Bericht");

  act(() => {
    fireEvent.change(input, { target: { value: "jorge@test.be" } });
    fireEvent.change(textarea, {
      target: { value: "jorge is de beste programmeur ooit" },
    });
  });

  const button = getByText("Verzend");
  fireEvent.click(button);

  await waitFor(() => {
    expect(
      queryByText("Hey, ik wil je wel terugmailen, hé")
    ).not.toBeInTheDocument();
    expect(queryByText("Wil je niet met mij spreken?")).not.toBeInTheDocument();
  });
});

/* 
1. error.email on regex match
2. alert on submit
*/
