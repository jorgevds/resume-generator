import React from "react";

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      return (
        <section className="app-letter error" data-testid="boundary">
          <h1 className="app-title">Something went wrong.</h1>
          <h2 className="app-title">Please refresh and try again.</h2>
          <h3 className="app-title">(We promise we know what we're doing.)</h3>
        </section>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
