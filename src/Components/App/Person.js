import React, { useState, useEffect } from "react";
import CoverLetter from "./CoverLetter";
import CV from "./Resume";
import Placeholder from "../Placeholder";
import Form from "../Contact/Form";
import { randomUser } from "../../Constants/fetch";

import ReactPlaceholder from "react-placeholder";
import "react-placeholder/lib/reactPlaceholder.css";

const Person = () => {
  const [url] = useState("https://randomuser.me/api/");
  const [person, setPerson] = useState(null);
  const [sex, setSex] = useState("man");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  // useEffect
  useEffect(() => {
    if ((!person && loading) || error) {
      const fetching = async () => {
        await randomUser(url)
          .then((data) => setPerson(data))
          .then((error) => setError(error));

        setTimeout(() => {
          setLoading(false);
        }, 2000);
      };
      fetching();
    }

    if (person != null && person.results[0].gender === "female") {
      setSex("vrouw");
    }
  }, [url, loading, person, error]);

  // useCallback
  // const getData = useCallback(async () => {
  //   const response = await fetch(url);
  //   const data = await response.json();
  //   setPerson(data.results);
  // }, [url]);

  // setTimeout(() => {
  //   setLoading(true);
  // }, 2000);

  // useEffect(() => {
  //   if (person.length === 0 && loading) {
  //     setLoading(false);
  //     getData();
  //   }
  //   console.log("effect", person);
  // }, [person]);
  // console.log(person);

  return (
    <ReactPlaceholder customPlaceholder={Placeholder} ready={!loading}>
      {person
        ? person.results.map((individual) => (
            <div key={individual.id.name} data-testid="person">
              <CoverLetter props={individual} sex={sex} />
              <CV props={individual} />
              <Form />
            </div>
          ))
        : null}
    </ReactPlaceholder>
  );
};

export default Person;
