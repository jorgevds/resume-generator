import React from "react";
import PropTypes from "prop-types";

const CV = ({ props }) => {
  return (
    <section className="app-resume" id="resume" data-testid="resume">
      <article className="letter-text">
        <h1>CV</h1>
        <p>Hieronder mijn gegevens</p>
      </article>
      <table>
        <thead>
          <tr>
            <th>Voornaam</th>
            <th>Naam</th>
            <th>Geslacht</th>
            <th>Leeftijd</th>
            <th>Stad</th>
            <th>Land</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{props.name.first}</td>
            <td>{props.name.last}</td>
            <td>{props.gender}</td>
            <td>{props.dob.age}</td>
            <td>{props.location.city}</td>
            <td>{props.location.country}</td>
          </tr>
        </tbody>
      </table>
    </section>
  );
};

CV.propTypes = {
  dob: PropTypes.shape({
    age: PropTypes.number,
  }),
};

export default CV;
