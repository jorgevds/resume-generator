import React from "react";
import CV from "../Resume";
import { render } from "@testing-library/react";

test("Resume component renders", () => {
  const mockData = {
    name: { first: "Karl", last: "Jobst" },
    gender: "male",
    dob: { age: 10 },
    location: { city: "Queensland", country: "Australia" },
  };

  const { getByTestId } = render(<CV props={mockData} />);

  const wrapper = getByTestId("resume");

  expect(wrapper).toBeInTheDocument();
});
