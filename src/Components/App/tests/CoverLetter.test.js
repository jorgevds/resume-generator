import React from "react";
import CoverLetter from "../CoverLetter";
import { render } from "@testing-library/react";

test("Renders cover letter component", () => {
  const mockData = {
    name: { first: "Karl", last: "Jobst" },
    picture: { large: "" },
    gender: "male",
    dob: { age: 10 },
    location: { city: "Queensland", country: "Australia" },
  };

  const { getByTestId } = render(<CoverLetter props={mockData} />);

  const wrapper = getByTestId("cover-letter");

  expect(wrapper).toBeInTheDocument();
});

test("Correctly evaluates props.sex as male", () => {
  const mockData = {
    name: { first: "Karl", last: "Jobst" },
    picture: { large: "" },
    dob: { age: 10 },
    location: { city: "Queensland", country: "Australia" },
  };

  const { getByTestId } = render(<CoverLetter props={mockData} sex="man" />);

  const sex = getByTestId("sex");

  expect(sex).toHaveTextContent("man");
});

test("Correctly evaluates props.sex as female", () => {
  const mockData = {
    name: { first: "Karl", last: "Jobst" },
    picture: { large: "" },
    dob: { age: 10 },
    location: { city: "Queensland", country: "Australia" },
  };

  const { getByTestId } = render(<CoverLetter props={mockData} sex="vrouw" />);

  const sex = getByTestId("sex");

  expect(sex).toHaveTextContent("vrouw");
});
