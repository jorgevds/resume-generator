import React from "react";
import { render, act } from "@testing-library/react";
import Person from "../Person";

import ReactPlaceholder from "react-placeholder";
import Placeholder from "../../Placeholder";
import "react-placeholder/lib/reactPlaceholder.css";

import { randomUser } from "../../../Constants/fetch";

test("Component does not render", () => {
  const { queryByTestId } = render(<Person />);

  const personWrapper = queryByTestId("person");
  expect(personWrapper).toBe(null);
});

test("Fetch function is called and called once", async () => {
  const mockData = {
    name: { first: "Karl", last: "Jobst" },
    picture: { large: "" },
    gender: "male",
    dob: { age: 10 },
    location: { city: "Queensland", country: "Australia" },
  };

  render(<Person />);

  await act(async () => {
    const dataFetch = jest.spyOn(global, "fetch").mockResolvedValue({
      json: jest.fn().mockResolvedValue({ mockData }),
    });
    randomUser();
    expect(dataFetch).toHaveBeenCalled();
    expect(dataFetch).toHaveBeenCalledTimes(1);
  });
});

test("Skeleton is rendered", async () => {
  const { queryByTestId } = render(
    <ReactPlaceholder customPlaceholder={Placeholder} ready={false} />
  );

  const skeleton = queryByTestId("skeleton");
  expect(skeleton).toBeInTheDocument();
});

test("Skeleton fades after n seconds", () => {
  jest.useFakeTimers();
  let loading = true;
  let n = 1000;

  render(<ReactPlaceholder loading={!loading} />);

  act(() => {
    setTimeout(() => {
      return (loading = false);
    }, n);

    jest.runAllTimers();
  });

  expect(loading).toBe(false);
});

/* 
1. data => setPerson(data)
2. error => setError(error)
3. setTimeout (although this is tested above)
4. setSex("vrouw")
*/
