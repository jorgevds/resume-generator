import React from "react";

const CoverLetter = ({ props, sex }) => {
  const name = props.name.first + " " + props.name.last;
  const age = props.dob.age + "-jarige";

  return (
    <section className="app-letter intro" data-testid="cover-letter">
      <h1 className="app-title">Hallo,</h1>
      <article className="letter-text">
        <p>Ik heet {name}</p>
        <p>
          Ik ben een {age} <span data-testid="sex">{sex}</span>
        </p>
        <p>
          Ik woon te {props.location.city} in {props.location.country}
        </p>
      </article>
      <img
        src={props.picture.large}
        alt={"Photo of " + props.name.first + props.name.last}
        width="200px"
        height="200px"
      />
    </section>
  );
};

export default CoverLetter;
