import { TextBlock, RectShape } from "react-placeholder/lib/placeholders";
import "../App.css";

const Placeholder = (
  <>
    <div className="person-placeholder" data-testid="skeleton">
      <TextBlock rows={7} style={{ width: 400 }} />
      <RectShape style={{ width: 200, height: 200 }} />
    </div>
    <div className="cv-placeholder">
      <TextBlock rows={7} />
    </div>
    <div className="form-placeholder">
      <RectShape style={{ height: 500 }} />
    </div>
  </>
);

export default Placeholder;
