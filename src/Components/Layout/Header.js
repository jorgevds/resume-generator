/* istanbul ignore file */
import React from "react";

const Header = () => {
  return (
    <header className="app-header">
      <nav id="home">
        <ul>
          <li>
            <a href="#">Home</a>
          </li>
          <li>
            <a href="#resume">Resume</a>
          </li>
          <li>
            <a href="#contact">Contact</a>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Header;
