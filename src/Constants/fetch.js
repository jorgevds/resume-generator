export const randomUser = async (url) => {
  try {
    const response = await fetch(url, {
      method: "GET",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "Application/json",
      },
      referrerPolicy: "no-referrer",
    });

    return response.json();
  } catch (error) {
    return error;
  }
};
