/* istanbul ignore file */
import "./App.css";
import Person from "./Components/App/Person";
import Header from "./Components/Layout/Header";
import ErrorBoundary from "./Components/ErrorBoundary";

function App() {
  return (
    <div className="App">
      <Header />
      <ErrorBoundary>
        <Person />
      </ErrorBoundary>
    </div>
  );
}

export default App;
